# kickstart file for the pysprings remix base
# based heavily on the Fedora XFCE spin (https://git.fedorahosted.org/git/spin-kickstarts.git)
#
# This is _not_ supported by the Fedora project. Please file any issues against the remix:
#  https://bitbucket.org/pysprings/fedora-remix

# Firewall configuration
firewall --enabled --service=mdns

# X Window System configuration information
xconfig  --startxonboot
repo --name="fedora" --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
repo --name="updates" --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f$releasever&arch=$basearch
repo --name="pysprings" --baseurl=http://pysprings.org/repo/$releasever/noarch

# Keyboard layouts
keyboard 'us'
# System timezone
timezone US/Mountain
# System language
lang en_US.UTF-8
# System authorization information
auth --useshadow --enablemd5
# SELinux configuration
selinux --enforcing

# System services
services --disabled="network,sshd" --enabled="NetworkManager"


%packages

# taken from the Fedora XFCE spin
@anaconda-tools
@base-x
@core
@fonts
@guest-desktop-agents
@hardware-support
@input-methods
@printing
@standard
@xfce-apps
@xfce-desktop
@xfce-extra-plugins
anaconda
gnome-keyring-pam
kernel
memtest86+
qemu-guest-agent
-PackageKit*
-acpid
-aspell-*
-autofs
-coolkey
-desktop-backgrounds-basic
-foomatic
-foomatic-db-ppds
-gimp-help
-gnumeric
-hpijs
-hplip
-ibus-typing-booster
-isdn4k-utils
-mpage
-numactl
-policycoreutils-gui
-ql2100-firmware
-ql2200-firmware
-ql23xx-firmware
-realmd
-sane-backends
-sox
-stix-fonts
-system-config-boot
-system-config-lvm
-system-config-network
-system-config-rootpassword
-wget
-xfce4-sensors-plugin
-xsane
-xsane-gimp

# make sure we don't get the fedora-specific packages
-fedora-release
-fedora-logos
-fedora-release-notes
generic-release
generic-logos
generic-release-notes

# remove stuff from the XFCE spin that we don't want
-pidgin
-transmission
-remmina
-claws-mail*
-liferea
-orage
-midori
-desktop-backgrounds-compat

# pysprings livecd
git
vim
python-virtualenv
python-ipython
firefox
libreoffice-impress
pysprings-wallpaper
pysprings-config

# pypsringboard
pyspringboard
gnupg

%end
