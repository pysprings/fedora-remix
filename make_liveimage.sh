VERSION='19.0'
RELEASEVER='19'
TITLE="PySprings Live $VERSION"
PRODUCT="pysprings-$VERSION"

sudo livecd-creator -d -v --cache=/root/livecd-creator-cache/ --logfile=livecdreator.log --title="$TITLE" --product="$PRODUCT"  --releasever=$RELEASEVER -c $1

